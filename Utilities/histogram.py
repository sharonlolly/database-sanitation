import pandas as pd
import matplotlib
from Utilities.Utilities import sensitive_rules_generator, all_rules_generator


def count_changes_in_columns(df,columns_values):
    res = []
    value = '???'
    for col in columns_values:
        if value not in df[col].unique().tolist():
            res.append(0)
        else:
            res.append(df[col].value_counts()[value])
    return res


def dissimilarity(mdsrrc_df, dsrrc_df,database_size):
    columns_values = list(mdsrrc_df.columns.values)
    raw_data = {}
    mdsrrc_diff = count_changes_in_columns(mdsrrc_df,columns_values)
    dsrrc_diff = count_changes_in_columns(dsrrc_df,columns_values)
    raw_data['DSRRC'] = dsrrc_diff
    raw_data['MDSRRC'] = mdsrrc_diff
    d = pd.DataFrame(raw_data, index=columns_values)
    plot = d.plot(kind='bar')
    fig = plot.get_figure()
    fig.savefig('Images and Histograms/dissimilarity.png')
    raw_data['DSRRC'] = sum(dsrrc_diff)/database_size
    raw_data['MDSRRC'] = sum(mdsrrc_diff)/database_size
    d2 = pd.DataFrame(raw_data, index=['diff'])
    plot2 = d2.plot(kind='bar')
    fig2 = plot2.get_figure()
    fig2.savefig('Images and Histograms/difference_in_percent.png')
    return fig,fig2


def side_effect_factor(original_bd, mdsrrc_db, dsrrc_db, mst, mct):
    raw_data={}
    columns_values = ['HF', 'MC', 'AP', 'SEF']
    all_original_rules = set(all_rules_generator(original_bd, mst, mct))
    sensitive_rules = set(sensitive_rules_generator(original_bd, mst, mct, {'sex', 'race'}))
    all_mdsrrc_rules = set(all_rules_generator(mdsrrc_db, mst, mct))
    all_dsrrc_rules = set(all_rules_generator(dsrrc_db, mst, mct))

    mdsrrc_values = {'HF':0, 'MC':0, 'AP':0,'SEF':0}
    dsrrc_values = {'HF':0, 'MC':0, 'AP':0,'SEF':0}

    # find hiding failures
    mdsrrc_values['HF'] = len(sensitive_rules.intersection(all_mdsrrc_rules))
    dsrrc_values['HF'] = len(sensitive_rules.intersection(all_dsrrc_rules))

    # find missing cost
    mdsrrc_values['MC'] = len(all_original_rules.difference(all_mdsrrc_rules))
    dsrrc_values['MC'] = len(all_original_rules.difference(all_dsrrc_rules))

    # find artificial patterns
    mdsrrc_values['AP'] = len(all_mdsrrc_rules.difference(all_original_rules))
    dsrrc_values['AP'] = len(all_dsrrc_rules.difference(all_dsrrc_rules))

    # find side effect factor
    mdsrrc_values['SEF'] = mdsrrc_values['HF']+ mdsrrc_values['MC']+mdsrrc_values['AP']
    dsrrc_values['SEF'] = dsrrc_values['HF']+ dsrrc_values['MC']+dsrrc_values['AP']

    raw_data['DSRRC'] = list(dsrrc_values.values())
    raw_data['MDSRRC'] = list(mdsrrc_values.values())
    d = pd.DataFrame(raw_data, index=columns_values)
    plot = d.plot(kind='bar')
    fig = plot.get_figure()
    fig.savefig('Images and Histograms/side_effect_factor.png')
    return fig
