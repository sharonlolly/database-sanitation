import numpy as np
import pandas as pd
from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules
from mlxtend.preprocessing import TransactionEncoder

from Objects.Database import Database
from Objects.Item import Item
from Objects.Rule import Rule


def find(predicate, l):
    """Return first item in sequence where f(item) == True."""
    for x in l:
        if predicate(x):
            return x
    return None


def association_rule_mining(data, minimum_support_threshold, minimum_confidence_threshold):
    """
    DESCRIPTION: Apply apriori algorithm on given database.
    Generate all possible association rules R.

    INPUT: A  data attribute of a Database object

    OUTPUT: all possible association rules R

    EXAMPLE:
    """
    te = TransactionEncoder()
    te_ary = te.fit(data).transform(data)
    df = pd.DataFrame(te_ary, columns=te.columns_)
    frequent_item_sets = apriori(df, min_support=minimum_support_threshold, use_colnames=True)
    rules = association_rules(frequent_item_sets, metric="confidence", min_threshold=minimum_confidence_threshold)
    columns_to_remove = ['antecedent support', 'consequent support', 'lift', 'leverage', 'conviction']
    rules.drop(columns=columns_to_remove, axis=1, inplace=True)
    rules_as_list = np.asarray(rules).tolist()
    rules_as_list = [[set(attribute) if type(attribute) is frozenset else round(attribute, 3) for attribute in rule] for rule in rules_as_list]
    return rules_as_list


def association_rule_reduction(association_rule_list, rule_antecedents_targets, rule_consequents_targets):
    """
    DESCRIPTION: reduces the list of association rules to rules that
    include rule_antecedents_targets in their R.H.S
    and include rule_consequents_targets in their L.H.S
    concluding in a much nero association rule list

    INPUT:  An Association Rule List
            An Antecedents Items set
            An Consequents Items set

    OUTPUT: A Reduced Association Rule List

    EXAMPLE:
    """
    reduced_AR_list = [rule for rule in association_rule_list if (rule[0].issuperset(rule_antecedents_targets) and rule[1].issuperset(rule_consequents_targets))]
    return reduced_AR_list


def sensitive_rule_selection(df, association_rule_list, sensetive_atrributes):
    """
    DESCRIPTION: Select set of rules SR ⊆ R as sensitive rules.

    INPUT: Association Rule List , data as dataframe

    OUTPUT: sensitive association rules list

    EXAMPLE:
    """
    sensitive_set = set()
    for attribute in sensetive_atrributes:
        unique_col_att = df[attribute].unique().tolist()
        sensitive_set |= (set(unique_col_att))  # union of all sensitive items
    sensitive_rules = [rule for rule in association_rule_list if (len(rule[0].intersection(sensitive_set)) != 0 or len(rule[1].intersection(sensitive_set)) != 0)]
    return sensitive_rules


def init_sensitive_items(sensitive_rules):
    """
    DESCRIPTION: Calculate sensitivity of each item in Database.

    INPUT:

    OUTPUT:

    EXAMPLE:
    """
    rules_items = set()
    for rule in sensitive_rules:
        rules_items |= rule.items

    list_of_sensitive_items = []
    for item in rules_items:
        list_of_sensitive_items.append(Item(item))

    return list_of_sensitive_items


def calculate_sensitivity_of_each_item(sensitive_rules, list_of_sensitive_items):
    """
    DESCRIPTION: Calculate sensitivity of each item in Database.

    INPUT:

    OUTPUT:

    EXAMPLE:
    """
    for rule in sensitive_rules:
        rule_items = rule.items
        for item_id in rule_items:
            item_obj = find(lambda x: x.item_id == item_id, list_of_sensitive_items)
            item_obj.sensitivity += 1


def init_support_confidence_for_rules(sensitive_rules, sensitive_transactions, database_size):
    for rule in sensitive_rules:
        for transaction in sensitive_transactions:
            if rule.items.issubset(transaction.item_list):
                rule.support_count += 1
            if rule.lhs.issubset(transaction.item_list):
                rule.lhs_count += 1
        rule.calc_confidence()
        rule.calc_support(database_size)


def update_support_confidence(sensitive_rules, transaction, item_to_be_deleted, database_size):
    for rule in sensitive_rules:
            if item_to_be_deleted.item_id in rule.items and rule.items.issubset(transaction.item_list):
                rule.support_count -= 1
            if item_to_be_deleted.item_id in rule.lhs and rule.lhs.issubset(transaction.item_list):
                rule.lhs_count -= 1
            rule.calc_support(database_size)
            rule.calc_confidence()


def generate_sanitized_database(sensitive_transactions, unsensitive_transaction, database_columns, file_name):
    sensitive_transactions = list(map(lambda t: t.item_list, sensitive_transactions))
    unsensitive_transaction.extend(sensitive_transactions)
    df = pd.DataFrame(unsensitive_transaction, columns=database_columns)
    file_name = "Sanitized Database/"+file_name
    df.to_csv(file_name, encoding='utf-8', index=False)
    database = Database(file_name)
    return database


def sensitive_rules_with_single_rhs_lhs(sensitive_rules):
    reduced_sensitive_rules_list = []
    for rule in sensitive_rules:
        if len(rule.rhs) == 1 and len(rule.lhs) == 1:
            reduced_sensitive_rules_list.append(rule)
    return reduced_sensitive_rules_list


def calculate_actual_support(data, list_of_sensitive_items):
    for item in list_of_sensitive_items:
        for row in data:
            if item.item_id in row:
                item.actual_support += 1


def association_rule_reduction(rules, lhs, rhs):
    reduced_AR_list = []
    for rule in rules:
        if rhs.issubset(rule.rhs) and lhs.issubset(rule.lhs):
            reduced_AR_list.append(rule)
    return reduced_AR_list


def sensitive_rules_generator(database, minimum_support_threshold, minimum_confidence_threshold, sensetive_atrributes):
    rules_as_list = association_rule_mining(database.data, minimum_support_threshold, minimum_confidence_threshold)
    sensitive_rules = sensitive_rule_selection(database.df, rules_as_list, sensetive_atrributes)
    sensitive_rules = list(map(lambda rule: Rule(rule[0], rule[1], rule[2], rule[3]), sensitive_rules))
    return sensitive_rules


def all_rules_generator(database, minimum_support_threshold, minimum_confidence_threshold):
    rules_as_list = association_rule_mining(database.data, minimum_support_threshold, minimum_confidence_threshold)
    all_rules = list(map(lambda rule: Rule(rule[0], rule[1], rule[2], rule[3]), rules_as_list))
    return all_rules