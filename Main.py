import copy
from Algorithms.DSRRC import DSRRC
from Algorithms.MDSRRC import MDSRRC
from Utilities.histogram import *
from Utilities.Utilities import *
from Objects.Database import Database


def main():
    # create a database object and alter the data as you wish
    database = Database("Original Database\ adult.csv")
    columns_to_remove = ['capital-gain', 'capital-loss', 'fnlwgt', 'education-num']
    database.omit_columns(columns_to_remove)
    columns_to_unify = ['age', 'hours-per-week']
    margin = [5, 10]
    database.unification(columns_to_unify, margin)

    # choose desired mst and mct
    minimum_support_threshold = 0.3
    minimum_confidence_threshold = 0.3

    # choose sensitive criteria and generate sensitive rules
    # (other selecting options are listed in the projectPresention nootbook)
    sensitive_rules = sensitive_rules_generator(database, minimum_support_threshold, minimum_confidence_threshold,{'marital-status'})
    sensitive_rules = sensitive_rules_with_single_rhs_lhs(sensitive_rules)
    database_size=database.df.size
    d1 = copy.deepcopy(database)
    d2 = copy.deepcopy(database)
    sr1 = copy.deepcopy(sensitive_rules)

    # running the algorithms
    dsrrc_db = DSRRC(d1, minimum_support_threshold, minimum_confidence_threshold, sensitive_rules)
    mdsrrc_db = MDSRRC(d2, minimum_support_threshold, minimum_confidence_threshold, sr1)

    # generating histograms comparing results are save in Images and Histograms directory
    plt1, plt3 = dissimilarity(mdsrrc_db.df,dsrrc_db.df,database_size)
    plt2 = side_effect_factor(database,mdsrrc_db, dsrrc_db, minimum_support_threshold,minimum_confidence_threshold)
    print("done")


if __name__ == "__main__":
    main()

