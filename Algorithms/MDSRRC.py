from operator import attrgetter

from Objects.Transaction import Transaction
from Utilities.Utilities import *


def MDSRRC(database, minimum_support_threshold, minimum_confidence_threshold, sensitive_rules_list = None):
    sensitive_rules = sensitive_rules_list or sensitive_rules_generator(database, minimum_support_threshold, minimum_confidence_threshold)
    list_of_sensitive_items = init_sensitive_items(sensitive_rules)
    calculate_sensitivity_of_each_item(sensitive_rules, list_of_sensitive_items)
    sensitive_transactions, unsensitive_transaction = init_transactions(list_of_sensitive_items, database.data)
    calculate_sensitivity_of_each_transaction(sensitive_transactions, list_of_sensitive_items)
    calculate_occurrence_in_rhs(list_of_sensitive_items, sensitive_rules)
    calculate_actual_support(database.data, list_of_sensitive_items)
    init_support_confidence_for_rules(sensitive_rules, sensitive_transactions, len(database.data))
    is_list = sorted(list_of_sensitive_items, key=attrgetter('occurrence_in_RHS', 'actual_support'), reverse=True)
    is0 = is_list[0]
    selected_transactions = get_transactions_support_is0(sensitive_transactions, is0)
    selected_transactions = sorted(selected_transactions, key=attrgetter('sensitivity', 'len'), reverse=True)
    index = 0
    while len(sensitive_rules) > 0:
        rule_was_deleted = False
        current_transaction = selected_transactions[index]
        update_support_confidence(sensitive_rules, current_transaction, is0, len(database.data))
        current_transaction.delete_item(is0)
        is0.actual_support -= 1
        for rule in list(sensitive_rules):
            if rule.is_hidden(minimum_support_threshold, minimum_confidence_threshold):
                rule_was_deleted = True
                update_sensitivity_rule_items(rule, list_of_sensitive_items)
                sensitive_rules.remove(rule)
                is_list = sorted(list_of_sensitive_items, key=attrgetter('occurrence_in_RHS', 'actual_support'), reverse = True)
                is0 = is_list[0]
                calculate_sensitivity_of_each_transaction(sensitive_transactions, list_of_sensitive_items)
                selected_transactions = get_transactions_support_is0(sensitive_transactions, is0)
                selected_transactions = sorted(selected_transactions, key=attrgetter('sensitivity', 'len'), reverse=True)
                index = 0
        if not rule_was_deleted:
            index += 1

    return generate_sanitized_database(sensitive_transactions, unsensitive_transaction, database.columns_values, 'mdsrrc.csv')

def init_transactions(list_of_sensitive_items, data):
    items_id = [item.item_id for item in list_of_sensitive_items]
    sensitive_transaction = []
    unsensitive_transaction = []
    for transaction in data:
        if len(set(items_id).intersection(transaction)) != 0:
            sensitive_transaction.append(Transaction(transaction))
        else:
            unsensitive_transaction.append(transaction)
    return sensitive_transaction, unsensitive_transaction


def update_sensitivity_rule_items(rule, list_of_sensitive_items):
    for item_id in rule.items:
        item_obj = find(lambda x: x.item_id == item_id, list_of_sensitive_items)
        item_obj.sensitivity -= 1
        if item_id in rule.rhs:
            item_obj.occurrence_in_RHS -= 1


def calculate_sensitivity_of_each_transaction(sensitive_transactions, list_of_sensitive_items):
    items_id = [item.item_id for item in list_of_sensitive_items]
    for transaction in sensitive_transactions:
        sensitive_items_intersection = set(transaction.item_list).intersection(items_id)
        sensitivity = 0
        for item_id in sensitive_items_intersection:
            item_obj = find(lambda x: x.item_id == item_id, list_of_sensitive_items)
            sensitivity += item_obj.sensitivity
        transaction.sensitivity = sensitivity
        # if transaction.sensitivity == 0:
        #     sensitive_transactions.remove(transaction)


def calculate_occurrence_in_rhs(list_of_sensitive_items, sensitive_rules):
    for rule in sensitive_rules:
        RHS = rule.rhs
        for item_id in RHS:
            item_obj = find(lambda x: x.item_id == item_id, list_of_sensitive_items)
            item_obj.occurrence_in_RHS += 1


def get_transactions_support_is0(sensitive_transactions, is0):
    selected_transactions = []
    for transaction in sensitive_transactions:
        if is0.item_id in transaction.item_list:
            selected_transactions.append(transaction)
    return selected_transactions
