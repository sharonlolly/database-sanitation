from operator import attrgetter

from Objects.Cluster import Cluster
from Objects.ClusterItem import ClusterItem
from Objects.ClusterTransaction import ClusterTransaction
from Utilities.Utilities import *


def DSRRC(database, minimum_support_threshold, minimum_confidence_threshold, sensitive_rules_list = None):
    sensitive_rules = sensitive_rules_list or sensitive_rules_generator(database, minimum_support_threshold, minimum_confidence_threshold)
    cluster_list = init_clusters(sensitive_rules)
    cluster_num = len(cluster_list)
    list_of_sensitive_items = init_sensitive_ClusterItems(sensitive_rules, cluster_num)
    set_sensitivity_for_clusters_items(cluster_list, list_of_sensitive_items)
    calc_rules_sensitivity_in_clusters(cluster_list, list_of_sensitive_items)
    calc_clusters_sensitivity(cluster_list)
    sensitive_transactions, unsensitive_transaction = init_transactions(list_of_sensitive_items, database.data, cluster_num)
    calc_transactions_sensitivity(sensitive_transactions, list_of_sensitive_items)
    add_transactions_to_cluster(cluster_list, sensitive_transactions)

    init_support_confidence_for_rules(sensitive_rules, sensitive_transactions, len(database.data))
    cluster_list = sorted(cluster_list, key=attrgetter('sensitivity'), reverse=True)

    for cluster in cluster_list:
        cluster.sort_transactions()
        deleted_item = find(lambda x: x.item_id == cluster.common_rhs, list_of_sensitive_items)
        while len(cluster.cluster_rules) > 0:
            current_transaction = cluster.transaction_list.pop()
            current_transaction.dec_sensitivity(deleted_item)
            update_support_confidence(sensitive_rules, current_transaction, deleted_item, len(database.data))
            current_transaction.delete_item(deleted_item)
            for rule in cluster.cluster_rules:
                if rule.is_hidden(minimum_support_threshold, minimum_confidence_threshold):
                    cluster.cluster_rules.remove(rule)

    return generate_sanitized_database(sensitive_transactions, unsensitive_transaction, database.columns_values,'dsrrc.csv')




def init_clusters(sensitive_rules):
    cluster_id = 0
    cluster_list = []
    for rule in sensitive_rules:
        cluster = find(lambda c: c.common_rhs == list(rule.rhs)[0], cluster_list)
        if cluster is None:
            cluster = Cluster(cluster_id, list(rule.rhs)[0])
            cluster_id += 1
            cluster_list.append(cluster)
        cluster.add_rule(rule)
    return cluster_list


def init_sensitive_ClusterItems(sensitive_rules, cluster_num):
    """
    DESCRIPTION: Calculate sensitivity of each item in Database.

    INPUT:

    OUTPUT:

    EXAMPLE:
    """
    rules_items = set()
    for rule in sensitive_rules:
        rules_items |= rule.items

    list_of_sensitive_items = []
    for item in rules_items:
        list_of_sensitive_items.append(ClusterItem(item, cluster_num))

    return list_of_sensitive_items


def init_transactions(list_of_sensitive_items, data, cluster_num):
    items_id = [item.item_id for item in list_of_sensitive_items]
    sensitive_transaction = []
    unsensitive_transaction = []
    for transaction in data:
        if len(set(items_id).intersection(transaction)) != 0:
            sensitive_transaction.append(ClusterTransaction(transaction, cluster_num))
        else:
            unsensitive_transaction.append(transaction)
    return sensitive_transaction, unsensitive_transaction


def set_sensitivity_for_clusters_items(cluster_list, list_of_sensitive_items):
    for cluster in cluster_list:
        for rule in cluster.cluster_rules:
            for item_id in rule.items:
                item_obj = find(lambda x: x.item_id == item_id, list_of_sensitive_items)
                item_obj.sensitivity[cluster.cluster_id] += 1


def calc_transactions_sensitivity(sensitive_transactions, list_of_sensitive_items):
    items_id = [item.item_id for item in list_of_sensitive_items]
    for transaction in sensitive_transactions:
        items = set(items_id).intersection(transaction.item_list)
        for item_id in items:
            item_obj = find(lambda x: x.item_id == item_id, list_of_sensitive_items)
            transaction.add_sensitivity(item_obj)


def add_transactions_to_cluster(cluster_list, sensitive_transactions):
    for cluster in cluster_list:
        for transaction in sensitive_transactions:
            if cluster.common_rhs in transaction.item_list:
                cluster.add_transaction(transaction)


def calc_rules_sensitivity_in_clusters(cluster_list, list_of_sensitive_items):
    for cluster in cluster_list:
        for rule in cluster.cluster_rules:
            rule_sensitivity = 0
            for item_id in rule.items:
                item_obj = find(lambda x: x.item_id == item_id, list_of_sensitive_items)
                rule_sensitivity += item_obj.sensitivity[cluster.cluster_id]
            rule.sensitivity = rule_sensitivity


def calc_clusters_sensitivity(cluster_list):
    for cluster in cluster_list:
        cluster.calc_cluster_sensitivity()
