# Database Sanitation

__Programmers:__
- Sharon Lutati
- Guy Erez
 
__Project Description:__

The problem of association rule hiding can be describe as: 
Transform the original database such that data mining techniques will results only non
 sensitive rules and all sensitive rules must not mined from transformed database. 
This transformed database is known as __sanitized database.__

The problem is to transform database D into D′ in such way that, data mining results from D′
 will hide all sensitive rules.

__The Goal:__ to hide association rules as many as possible
by modifying the transactions, while minimizing side effects which can be generated
by hiding process. 

__The side effects__ can be generated by hiding process:
1. Some sensitive rules which can be mined from sanitized database, called __hiding
failure (HF)__ effect.
2. Some non-sensitive rules are hidden accidently in sanitized database, called lost
rule or __missing cost (MC)__  effect.
3. Some rules are newly created, called ghost rule or __artificial rule (AP)__  effect.


_Optimal Sanitation was proved to be a NP hard problem!_

__Basic concepts of Assosiation Rules:__

Association rule using support and confidence can be defined as follows.
Let I={i1,…,im} be a set of items. Database D={T1,⋯,Tn} is a set of transactions, 
where Ti⊆I(1≤i≤m). 

Each transaction T is an itemset such that T⊆I.
 A transaction T supports X, a set of items in I, if X⊆I. 
The association rule is an implication formula like X⇒Y, where X⊂I,Y⊂I and X∩Y=∅.

 The rule X⇒Y with support s and confidence c is called, 
 
 𝐈𝐅 (|𝑿∪𝒀|)/(|𝑫|)≥𝑺    𝑨𝑵𝑫     (|𝑿∪𝒀|)/(|𝑿|)≥𝑪

Because of interestingness, we consider user specified thresholds for support and confidence, 
_MST (minimum support threshold)_ and _MCT (minimum confidence threshold)_. 



__Algorithms Implemented:__
- DSRRC
    - Reference:  [DSRRC Algorithm](https://ieeexplore.ieee.org/abstract/document/5592589)
- MDRSRRC
    - Reference: [MDSRRC Algorithm](https://ieeexplore.ieee.org/document/6514417)

_Note: One can gain free access to website through a university network._

__Database Used:__

adult.data database, reference [here](https://archive.ics.uci.edu/ml/datasets/adult).

**Prediction task** is to determine whether a person makes over or under 50K a year.

_**Meaning:** For the purposes of our project, we will consider any association rule where : X ⇒ Salary_
Salary is the group {'<=50K' , '>50K'}.

*Sensitive rule we will want to remove would be rules that include sex or race.*

__General Notes About Implementation:__
 - Libraries used: [mlxtend](http://rasbt.github.io/mlxtend/user_guide/frequent_patterns/association_rules/) for association rules mining.
 - the algorithms run generate the sanitized data as a csv file and save it under the corresponding algorithm name in the directory 'Sanitized Database'.
 - the histograms comparing the algorithms results are saved in the directory 'Images and histograms'(please advise the article about the histograms).
 - the projectPresention jupyter book is added for a more convenient algorithm run.
    
__Personal Note:__ please consult us about questions or suggested improvements.
 - *Sharon Lutati :* sharonlolly@gmail.com
 - *Guy Erez :* guy.e12@gmail.com  
 