class Transaction:
    """
    A class used to represent a Transaction in database

    Attributes
    ----------
    item_list :     List[str]
                    List of item purchased in Transaction.
    sensitivity :   int
                    Sensitivity of Transaction: is the total of sensitivities of all
                    sensitive items which are presented in that transaction
    len :           int
                    Length of item_list.
    Methods
    -------
    delete_item(item)
        Delete an Item from transaction.
    """
    def __init__(self, item_list):
        """
        Parameters
        ----------
        item_list :     List[str]
                        List of item purchased in Transaction.
        """
        self.item_list = item_list
        self.sensitivity = 0
        self.len = len(item_list)

    def delete_item(self, item):
        """
        Parameters
        ----------
        item :      Item Object.
                    Item to be deleted from Transaction.
        """
        item_index = self.item_list.index(item.item_id)
        self.item_list.remove(item.item_id)
        self.item_list.insert(item_index, "???")

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self)
