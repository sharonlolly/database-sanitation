from operator import methodcaller

from Objects.Transaction import Transaction


class Cluster:

    def __init__(self, cluster_id, common_rhs):
        self.cluster_id = cluster_id
        self.cluster_rules = []
        self.common_rhs = common_rhs
        self.transaction_list = []
        self.sensitivity = 0

    def add_rule(self, rule):
        self.cluster_rules.append(rule)

    def add_transaction(self, transaction: Transaction):
        self.transaction_list.append(transaction)

    def calc_cluster_sensitivity(self):
        for rule in self.cluster_rules:
            self.sensitivity += rule.sensitivity

    def sort_transactions(self):
        self.transaction_list = sorted(self.transaction_list, key=methodcaller('get_sensitivity',self.cluster_id),reverse=True)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self)