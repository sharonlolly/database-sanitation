class Item:
    """
    A class used to represent an Item in database

    Attributes
    ----------
    item_id :    str
                id of Item / unique name of Item.
    sensitivity :   int
                    Item Sensitivity is the frequency of data item exists in
                    the number of the sensitive association rule containing this
                    item. It is used to measure rule sensitivity.
    actual_support:     int
                        the frequency of data item exists in database

    occurrence_in_RHS:  int
                        the frequency of data item exists in
                        the number of the sensitive association rule rhs.
    """

    def __init__(self, item_id):
        """
        Parameters
        ----------
        item_id : str
                individual identification of item in database.
        """
        self.item_id = item_id
        self.sensitivity = 0
        self.actual_support = 0
        self.occurrence_in_RHS = 0

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self)