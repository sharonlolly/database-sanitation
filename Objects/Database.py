import numpy as np
import pandas as pd


class Database:
    """
    A class used to represent a Database

    Attributes
    ----------
    df :    DataFrame from Pandas library,
            can be defined as a two-dimensional
            labeled data structures with columns of
            potentially different types.
    data :  List[List[Any]]
            representation of the DataFrame as a list of transaction.

    Methods
    -------
    omit_columns(columns_to_remove: List[str])
        remove columns from df DataFrame.
        and update df data representation respectively.

    unification(columns: List[str], margin: List[int])
        unify data on columns[i] by margin[i].
        update df data representation respectively.

    """
    def omit_columns(self, columns_to_remove):
        """
        remove columns from df DataFrame.
        and update df data representation respectively.

        Pre: columns_to_remove is subset of DataFrame columns.

        Parameters
        ----------
        columns_to_remove :     List[str]
                                DataFrame columns names to remove from df.
        """
        self.df = self.df.drop(columns=columns_to_remove, axis=1, inplace=False)
        self.data = self.df.values.tolist()
        self.columns_values = list(self.df.columns.values)

    def unification(self, columns, margin):
        """
        unify columns in df by margin specified.
        unify data on columns[i] by margin[i].
        update df data representation respectively.

        Pre:    len(columns)==len(margin)
                columns is subset of DataFrame columns.

        Parameters
        ----------
        columns :   List[str]
                    DataFrame columns names to unify.
        margin :    List[int]
                    margins to unify the columns Data by.
        """
        for i in range(0, len(columns)):
            column = self.df[columns[i]].astype(int)
            labels = ['{0}-{1}'.format(num, num + margin[i]) for num in range(min(column), max(column) + 1, margin[i])]
            bins = len(labels)
            self.df[columns[i]] = pd.cut(np.array(column), bins, labels=labels).astype(str)
        self.data = self.df.values.tolist()
        self.columns_values = list(self.df.columns.values)

    def __init__(self, path):
        """
        Parameters
        ----------
        path :  str
                path to data in csv format.
        """
        self.df = pd.read_csv(path, dtype=str)
        self.data = self.df.values.tolist()
        self.columns_values = list(self.df.columns.values)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self)
