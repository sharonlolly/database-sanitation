from Objects.Transaction import Transaction


class ClusterTransaction(Transaction):

    def __init__(self, item_list, cluster_num):
        Transaction.__init__(self, item_list)
        self.sensitivity = [0] * cluster_num
        self.cluster_num = cluster_num

    def get_sensitivity(self, cluster_num):
        return self.sensitivity[cluster_num]

    def add_sensitivity(self, cluster_item):
        for i in range(0, self.cluster_num):
            self.sensitivity[i] += cluster_item.sensitivity[i]

    def dec_sensitivity(self, deleted_item):
        for i in range(0, self.cluster_num):
            self.sensitivity[i] -= deleted_item.sensitivity[i]

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self)
