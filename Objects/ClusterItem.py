from Objects.Item import Item


class ClusterItem(Item):

    def __init__(self, item_id, cluster_num):
        Item.__init__(self, item_id)
        self.sensitivity = [0]*cluster_num

    def get_sensitivity(self, cluster_num):
        return self.sensitivity[cluster_num]

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self)
