class Rule:

    def __init__(self, lhs, rhs, support, confidence):
        self.lhs = lhs
        self.rhs = rhs
        self.items = rhs.union(lhs)
        self.support = support
        self.confidence = confidence
        self.support_count = 0
        self.lhs_count = 0
        self.sensitivity = 0

    def is_hidden(self, min_support, min_confidence):
        return (self.support < min_support) or (self.confidence < min_confidence)

    def calc_support(self, database_size):
        self.support = round(self.support_count/database_size, 3)

    def calc_confidence(self):
        self.confidence = round(self.support_count/self.lhs_count, 3)

    def __str__(self):
        return str({'lhs': self.lhs, 'rhs': self.rhs})

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(self.__repr__())

    def __eq__(self, other):
        if isinstance(other, Rule):
            return self.rhs == other.rhs and self.lhs == other.lhs
        else:
            return False
